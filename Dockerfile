FROM node:14-alpine

RUN mkdir -p /app
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm i

COPY tsconfig.json .eslintrc.js .prettierrc.yaml .eslintignore /app/
COPY src /app/src
COPY test /app/test
COPY config/config-example.yml /app/config/config-example.yml

RUN npm run build

CMD [ "npm", "run", "start" ]
