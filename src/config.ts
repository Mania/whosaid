import convict from 'convict'
import { safeLoad } from 'js-yaml'
import { join } from 'path'

convict.addParser({ extension: ['yml', 'yaml'], parse: safeLoad })

// Define a schema
const config = convict({
    discord: {
        token: {
            format: String,
            default: null,
        },
    },
    hasura: {
        host: {
            format: String,
            default: null,
        },
        httpProtocol: {
            format: String,
            default: 'http',
        },
        wsProtocol: {
            format: String,
            default: 'ws',
        },
        port: {
            format: Number,
            default: 80,
        },
        path: {
            format: String,
            default: '/',
        },
    },
})

// Load environment dependent configuration
config.loadFile(join(__dirname + '/../config/config.yml'))

// Perform validation
config.validate({
    allowed: 'strict',
})

//module.exports = config
export { config }
