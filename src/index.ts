import winstonLogger from './winston'
import { Container } from 'typedi'
import { SpyService } from './services/spy'

const logger = winstonLogger(__filename)

export async function main(): Promise<void> {
    logger.info('App launched')

    try {
        const spyService = Container.get(SpyService)

        await Promise.all([spyService.start()])

        // Sleep indefinitly
        await new Promise(() => {
            // do nothing.
        })
    } catch (e) {
        logger.error(e)
        process.exit(1)
    }
}

/* istanbul ignore if */
if (process.env.NODE_ENV !== 'test') {
    main().then(() => {
        logger.info('Exiting')
        process.exit(0)
    })
}
