import { createLogger, format, Logger, transports } from 'winston'

const colorizer = format.colorize()
export const loggerArray: Logger[] = []

const logger = (filename: string, logLevel?: string): Logger => {
    const logger = createLogger({
        level: logLevel || process.env.LOG_LEVEL || 'info',
        format: format.combine(
            format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
            format.printf((info) =>
                colorizer.colorize(
                    info.level,
                    `${info.timestamp} :: ${info.level} :: ${filename} : ${info.message}`
                )
            )
        ),
        transports: [new transports.Console()],
    })
    loggerArray.push(logger)
    return logger
}

export default logger
