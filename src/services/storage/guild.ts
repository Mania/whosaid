import 'reflect-metadata'
import { Service } from 'typedi'
import { StorageClientService } from './client'
import { default as lg } from '../../winston'
import { Guild, GuildMember } from 'discord.js'
import {
    InsertGuildMutation,
    InsertGuildMutationVariables,
    Member_Constraint,
    Member_Insert_Input,
    Member_Update_Column,
    QueryGuildByPkQuery,
    QueryGuildByPkQueryVariables,
} from '../../generated/graphql'
import { gql } from 'apollo-boost'

const logger = lg(__filename)

@Service()
export class GuildStorageService {
    constructor(private readonly storageClientService: StorageClientService) {}

    public async registerGuild(guild: Guild): Promise<void> {
        logger.verbose(`Registering guild ${guild.name}`)
        const guildExist = await this.guildExist(guild)

        if (guildExist) {
            logger.verbose(`Guild ${guild.name} already exist`)
            return
        }

        await this.insertGuild(guild)
    }

    public async guildExist({ id }: Guild): Promise<boolean> {
        const QUERY_GUILD_BY_PK = gql`
            query QueryGuildByPk($id: String!) {
                guild_by_pk(id: $id) {
                    id
                }
            }
        `

        const result = await this.storageClientService.client.query<
            QueryGuildByPkQuery,
            QueryGuildByPkQueryVariables
        >({
            query: QUERY_GUILD_BY_PK,
            variables: { id },
        })

        return result.data.guild_by_pk !== null
    }

    public async insertGuild(guild: Guild): Promise<void> {
        logger.verbose(`Inserting guild ${guild.name}`)

        const INSERT_GUILD = gql`
            mutation InsertGuild($guild_insert_input: guild_insert_input!) {
                insert_guild_one(object: $guild_insert_input) {
                    id
                }
            }
        `

        const membersData: Member_Insert_Input[] = guild.members.cache
            .array()
            .map(
                (member: GuildMember): Member_Insert_Input => ({
                    bannable: member.bannable,
                    display_name: member.displayName,
                    kickable: member.kickable,
                    manageable: member.manageable,
                    member_joined_at: member.joinedAt,
                    nickname: member.nickname,
                    user_id: member.id,
                })
            )

        await this.storageClientService.client.mutate<
            InsertGuildMutation,
            InsertGuildMutationVariables
        >({
            mutation: INSERT_GUILD,
            variables: {
                guild_insert_input: {
                    guild_created_at: guild.createdAt,
                    id: guild.id,
                    joined_at: guild.joinedAt,
                    memberCount: guild.memberCount,
                    mfaLevel: guild.mfaLevel,
                    name: guild.name,
                    owner_id: guild.ownerID,
                    region: guild.region,
                    members: {
                        data: membersData,
                        on_conflict: {
                            constraint: Member_Constraint.MemberPkey,
                            update_columns: [
                                Member_Update_Column.Nickname,
                                Member_Update_Column.Bannable,
                                Member_Update_Column.Kickable,
                                Member_Update_Column.Manageable,
                                Member_Update_Column.DisplayName,
                            ],
                        },
                    },
                },
            },
        })

        logger.verbose(`Guild ${guild.name} inserted`)
    }
}
