import 'reflect-metadata'
import { Service } from 'typedi'
import { StorageClientService } from './client'
import { default as lg } from '../../winston'
import { TextChannel } from 'discord.js'
import {
    InsertChannelMutation,
    InsertChannelMutationVariables,
    IsChannelHistoryCompleteQuery,
    IsChannelHistoryCompleteQueryVariables,
    QueryChannelByPkQuery,
    QueryChannelByPkQueryVariables,
} from '../../generated/graphql'
import { gql } from 'apollo-boost'

const logger = lg(__filename)

@Service()
export class ChannelStorageService {
    constructor(private readonly storageClientService: StorageClientService) {}

    public async registerChannel(channel: TextChannel): Promise<void> {
        logger.verbose(`Registering channel ${channel.name}`)

        const channelExist = await this.channelExist(channel)

        if (channelExist) {
            logger.verbose(`Guild ${channel.name} already exist`)
            return
        }

        await this.insertChannel(channel)
    }

    public async channelExist({ id }: TextChannel): Promise<boolean> {
        const QUERY_CHANNEL_BY_PK = gql`
            query QueryChannelByPk($id: String!) {
                channel_by_pk(id: $id) {
                    id
                }
            }
        `

        const result = await this.storageClientService.client.query<
            QueryChannelByPkQuery,
            QueryChannelByPkQueryVariables
        >({
            query: QUERY_CHANNEL_BY_PK,
            variables: { id: id },
        })

        return result.data.channel_by_pk !== null
    }

    public async insertChannel(channel: TextChannel): Promise<void> {
        logger.verbose(`Inserting channel ${channel.name}`)

        const INSERT_CHANNEL = gql`
            mutation InsertChannel(
                $channel_insert_input: channel_insert_input!
            ) {
                insert_channel_one(object: $channel_insert_input) {
                    id
                }
            }
        `

        await this.storageClientService.client.mutate<
            InsertChannelMutation,
            InsertChannelMutationVariables
        >({
            mutation: INSERT_CHANNEL,
            variables: {
                channel_insert_input: {
                    guild_id: channel.guild.id,
                    id: channel.id,
                    manageable: channel.manageable,
                    name: channel.name,
                    nsfw: channel.nsfw || false,
                    position: channel.position,
                    rawPosition: channel.rawPosition,
                    topic: channel.topic,
                    type: channel.type,
                    viewable: channel.viewable,
                    channel_created_at: channel.createdAt,
                },
            },
        })

        logger.verbose(`Channel ${channel.name} inserted`)
    }

    public async isChannelHistoryComplete({
        id,
    }: TextChannel): Promise<boolean> {
        const QUERY_HISTORY_COMPLETE = gql`
            query IsChannelHistoryComplete($id: String!) {
                channel_by_pk(id: $id) {
                    id
                    history_complete
                }
            }
        `

        const result = await this.storageClientService.client.query<
            IsChannelHistoryCompleteQuery,
            IsChannelHistoryCompleteQueryVariables
        >({
            query: QUERY_HISTORY_COMPLETE,
            variables: { id },
        })

        return result.data.channel_by_pk?.history_complete || false
    }
}
