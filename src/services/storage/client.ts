import 'reflect-metadata'
import { Service } from 'typedi'

import ApolloClient from 'apollo-client'
import 'cross-fetch/polyfill'
import * as url from 'url'
import { config } from '../../config'

import { WebSocketLink } from 'apollo-link-ws'
import { HttpLink } from 'apollo-link-http'
import { split } from 'apollo-link'
import { getMainDefinition } from 'apollo-utilities'
import { InMemoryCache } from 'apollo-cache-inmemory'
import websocket from 'ws'
import { NormalizedCacheObject } from 'apollo-cache-inmemory/lib/types'

@Service()
export class StorageClientService {
    private readonly _client: ApolloClient<NormalizedCacheObject>

    constructor() {
        const httpLink = new HttpLink({
            uri: url.format({
                protocol: config.get('hasura.httpProtocol'),
                hostname: config.get('hasura.host'),
                pathname: config.get('hasura.path'),
                port: config.get('hasura.port'),
            }),
        })

        const wsLink = new WebSocketLink({
            uri: url.format({
                protocol: config.get('hasura.wsProtocol'),
                hostname: config.get('hasura.host'),
                pathname: config.get('hasura.path'),
                port: config.get('hasura.port'),
            }),
            options: {
                reconnect: true,
            },
            webSocketImpl: websocket,
        })

        // using the ability to split links, you can send data to each link
        // depending on what kind of operation is being sent
        const link = split(
            // split based on operation type
            ({ query }) => {
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                const { kind, operation } = getMainDefinition(query)
                return (
                    kind === 'OperationDefinition' &&
                    operation === 'subscription'
                )
            },
            wsLink,
            httpLink
        )

        this._client = new ApolloClient<NormalizedCacheObject>({
            link,
            cache: new InMemoryCache(),
        })
    }

    public get client(): ApolloClient<NormalizedCacheObject> {
        return this._client
    }
}
