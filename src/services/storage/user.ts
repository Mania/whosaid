import 'reflect-metadata'
import { Service } from 'typedi'
import { StorageClientService } from './client'
import { default as lg } from '../../winston'
import { User } from 'discord.js'
import {
    InsertUserMutation,
    InsertUserMutationVariables,
    QueryUserByPkQuery,
    QueryUserByPkQueryVariables,
} from '../../generated/graphql'
import { gql } from 'apollo-boost'

const logger = lg(__filename)

@Service()
export class UserStorageService {
    constructor(private readonly storageClientService: StorageClientService) {}

    public async registerUser(user: User): Promise<void> {
        logger.verbose(`Registering user ${user.tag}`)
        const userExist = await this.userExist(user)

        if (userExist) {
            logger.verbose(`User ${user.tag} already exist`)
            return
        }

        await this.insertUser(user)
    }

    public async userExist({ id }: User): Promise<boolean> {
        const QUERY_USER_BY_PK = gql`
            query QueryUserByPk($id: String!) {
                user_by_pk(id: $id) {
                    id
                }
            }
        `

        const result = await this.storageClientService.client.query<
            QueryUserByPkQuery,
            QueryUserByPkQueryVariables
        >({
            query: QUERY_USER_BY_PK,
            variables: { id },
        })

        return result.data.user_by_pk !== null
    }

    public async insertUser(user: User): Promise<void> {
        logger.verbose(`Inserting user ${user.tag}`)

        const INSERT_USER = gql`
            mutation InsertUser($user_insert_input: user_insert_input!) {
                insert_user_one(object: $user_insert_input) {
                    id
                }
            }
        `

        await this.storageClientService.client.mutate<
            InsertUserMutation,
            InsertUserMutationVariables
        >({
            mutation: INSERT_USER,
            variables: {
                user_insert_input: {
                    id: user.id,
                    avatar: user.avatar,
                    bot: user.bot,
                    locale: user.locale,
                    tag: user.tag,
                    user_created_at: user.createdAt,
                    username: user.username,
                },
            },
        })

        logger.verbose(`User ${user.tag} inserted`)
    }
}
