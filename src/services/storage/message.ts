import 'reflect-metadata'
import { Service } from 'typedi'
import { StorageClientService } from './client'
import { default as lg } from '../../winston'
import { Message, TextChannel } from 'discord.js'
import {
    GetChannelOldestMessageQuery,
    GetChannelOldestMessageQueryVariables,
    InsertMessagesMutation,
    InsertMessagesMutationVariables,
    Message_Constraint,
    Message_Insert_Input,
    Message_Update_Column,
    SetChannelHistoryCompleteMutation,
    SetChannelHistoryCompleteMutationVariables,
} from '../../generated/graphql'
import { gql } from 'apollo-boost'

const logger = lg(__filename, 'debug')

@Service()
export class MessageStorageService {
    constructor(private readonly storageClientService: StorageClientService) {}

    public async registerChannelMessages(
        textChannel: TextChannel
    ): Promise<void> {
        logger.verbose(`Registering channel ${textChannel.name} messages`)

        let oldestChannelMessage = await this.getOldestChannelMessageId(
            textChannel
        )

        logger.debug(
            oldestChannelMessage
                ? `Last registered message for channel ${textChannel.name} was at ${oldestChannelMessage}`
                : `No last message registered for channel ${textChannel.name}`
        )
        let messagesArr: Message[] = []
        do {
            logger.debug(
                `Fetching messages before #${oldestChannelMessage} in ${textChannel.name}`
            )
            // Register messages
            const messages = await textChannel.messages.fetch({
                limit: 100,
                before: oldestChannelMessage || undefined,
            })
            messagesArr = messages.array()

            logger.debug(
                `${messagesArr.length} messages fetched in ${textChannel.name}`
            )
            await this.registerMessages(messagesArr)

            oldestChannelMessage = await this.getOldestChannelMessageId(
                textChannel
            )
        } while (messagesArr.length > 0)

        await this.setChannelHistoryComplete(textChannel)
        logger.info(`History complete for channel ${textChannel.name}`)
    }

    public async getOldestChannelMessageId({
        id,
        name,
    }: TextChannel): Promise<string | null> {
        logger.verbose(`Getting channel ${name} last message timestamp`)

        const QUERY_OLDEST_MESSAGE = gql`
            query getChannelOldestMessage($channel_id: String!) {
                message(
                    limit: 1
                    where: { channel_id: { _eq: $channel_id } }
                    order_by: { message_created_at: asc }
                ) {
                    id
                }
            }
        `

        const result = await this.storageClientService.client.query<
            GetChannelOldestMessageQuery,
            GetChannelOldestMessageQueryVariables
        >({
            query: QUERY_OLDEST_MESSAGE,
            variables: { channel_id: id },
            fetchPolicy: 'no-cache',
        })

        return result.data.message[0]?.id || null
    }

    private async registerMessages(messages: Message[]): Promise<void> {
        logger.verbose(`Registering ${messages.length} messages`)

        const INSERT_MESSAGES = gql`
            mutation InsertMessages(
                $messages: [message_insert_input!]!
                $on_conflict: message_on_conflict
            ) {
                insert_message(objects: $messages, on_conflict: $on_conflict) {
                    affected_rows
                }
            }
        `

        await this.storageClientService.client.mutate<
            InsertMessagesMutation,
            InsertMessagesMutationVariables
        >({
            mutation: INSERT_MESSAGES,
            variables: {
                messages: messages.map(
                    (message: Message): Message_Insert_Input => ({
                        channel_id: message.channel.id,
                        cleanContent: message.cleanContent,
                        content: message.content,
                        id: message.id,
                        message_created_at: message.createdAt,
                        tts: message.tts,
                        user_id: message.member?.id || null,
                    })
                ),
                on_conflict: {
                    constraint: Message_Constraint.MessagePkey,
                    update_columns: [
                        Message_Update_Column.CleanContent,
                        Message_Update_Column.Content,
                        Message_Update_Column.MessageCreatedAt,
                    ],
                },
            },
        })
    }

    public async setChannelHistoryComplete({
        id,
        name,
    }: TextChannel): Promise<void> {
        logger.debug(`Setting channel ${name} history complete`)
        const SET_CHANNEL_HISORY_COMPLETE = gql`
            mutation SetChannelHistoryComplete($channel_id: String!) {
                update_channel_by_pk(
                    pk_columns: { id: $channel_id }
                    _set: { history_complete: true }
                ) {
                    id
                }
            }
        `

        await this.storageClientService.client.mutate<
            SetChannelHistoryCompleteMutation,
            SetChannelHistoryCompleteMutationVariables
        >({
            mutation: SET_CHANNEL_HISORY_COMPLETE,
            variables: { channel_id: id },
        })
    }
}
