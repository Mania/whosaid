import 'reflect-metadata'
import { Service } from 'typedi'

import { Client } from 'discord.js'
import { config } from '../config'

@Service()
export class DiscordService {
    private client?: Client

    public async start(): Promise<Client> {
        this.client = new Client({
            partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
        })
        await this.client.login(config.get('discord.token'))
        return this.client
    }

    public async getClient(): Promise<Client> {
        return this.client || this.start()
    }
}
