import 'reflect-metadata'
import { DiscordService } from './discord'
import { Client, TextChannel } from 'discord.js'
import { GuildStorageService } from './storage/guild'
import { default as lg } from '../winston'
import { Service } from 'typedi'
import PQueue from 'p-queue'
import { UserStorageService } from './storage/user'
import { MessageStorageService } from './storage/message'
import { ChannelStorageService } from './storage/channel'

const logger = lg(__filename)

@Service()
export class SpyService {
    private client!: Client
    private userQueue: PQueue = new PQueue({ concurrency: 10 })
    private guildQueue: PQueue = new PQueue({ concurrency: 10 })
    private channelQueue: PQueue = new PQueue({ concurrency: 10 })
    private messageQueue: PQueue = new PQueue({ concurrency: 2 })

    constructor(
        private readonly discordService: DiscordService,
        private readonly guildStorageService: GuildStorageService,
        private readonly userStorageService: UserStorageService,
        private readonly messageStorageService: MessageStorageService,
        private readonly channelStorageService: ChannelStorageService
    ) {}

    public async start(): Promise<void> {
        this.client = await this.discordService.getClient()
        this.client.on('ready', () => {
            this.onReady()
        })
    }

    public async onReady(): Promise<void> {
        if (!this.client) {
            logger.error('Client does not exist. Please call start()')
        }

        logger.info(
            `Bot has started, with ${this.client.users.cache.size} users, in ${this.client.channels.cache.size} channels of ${this.client.guilds.cache.size} guilds.`
        )

        await this.spyUsers()
        await this.spyGuilds()
        await this.spyChannels()
        await this.spyMessages()
    }

    public async spyUsers(): Promise<void> {
        const userAddPromises = []

        for (const user of this.client.users.cache.values()) {
            const userAddPromise = this.userQueue.add(async () =>
                this.userStorageService.registerUser(user)
            )
            userAddPromises.push(userAddPromise)
        }

        await Promise.all(userAddPromises)
        logger.info('All initial users are registered')
    }

    public async spyGuilds(): Promise<void> {
        const guildAddPromises = []

        for (const guild of this.client.guilds.cache.values()) {
            const guildAddPromise = this.guildQueue.add(async () =>
                this.guildStorageService.registerGuild(guild)
            )
            guildAddPromises.push(guildAddPromise)
        }

        await Promise.all(guildAddPromises)
        logger.info('All initial guilds are registered')
    }

    public async spyChannels(): Promise<void> {
        const channelAddPromises = []
        for (const channel of this.client.channels.cache.values()) {
            if (channel.type !== 'text') {
                logger.debug(
                    `Ignoring channel ${channel.id} as it's type is ${channel.type}`
                )
                continue
            }

            const textChannel = channel as TextChannel

            const channelAddPromise = this.channelQueue.add(async () =>
                this.channelStorageService.registerChannel(textChannel)
            )
            channelAddPromises.push(channelAddPromise)
        }

        await Promise.all(channelAddPromises)
        logger.info('All initial channels are registered')
    }

    public async spyMessages(): Promise<void> {
        const messageAddPromises = []

        for (const channel of this.client.channels.cache.values()) {
            if (channel.type !== 'text') {
                logger.verbose(
                    `Ignoring channel ${channel.id} as it's type is ${channel.type}`
                )
                continue
            }

            const textChannel = channel as TextChannel

            if (
                await this.channelStorageService.isChannelHistoryComplete(
                    textChannel
                )
            ) {
                logger.verbose(
                    `Ignoring channel ${channel.id} as it's history is already complete`
                )
                continue
            }

            const channelAddPromise = this.messageQueue.add(async () =>
                this.messageStorageService.registerChannelMessages(textChannel)
            )
            messageAddPromises.push(channelAddPromise)
        }

        await Promise.all(messageAddPromises)
        logger.info('All initial messages are registered')
    }
}
