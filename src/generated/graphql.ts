import gql from 'graphql-tag'
import * as ApolloReactCommon from '@apollo/react-common'
export type Maybe<T> = T | null
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string
    String: string
    Boolean: boolean
    Int: number
    Float: number
    numeric: any
    timestamptz: any
}

/** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
    _eq?: Maybe<Scalars['Boolean']>
    _gt?: Maybe<Scalars['Boolean']>
    _gte?: Maybe<Scalars['Boolean']>
    _in?: Maybe<Array<Scalars['Boolean']>>
    _is_null?: Maybe<Scalars['Boolean']>
    _lt?: Maybe<Scalars['Boolean']>
    _lte?: Maybe<Scalars['Boolean']>
    _neq?: Maybe<Scalars['Boolean']>
    _nin?: Maybe<Array<Scalars['Boolean']>>
}

export type SampleInput = {
    password: Scalars['String']
    username: Scalars['String']
}

export type SampleOutput = {
    __typename?: 'SampleOutput'
    accessToken: Scalars['String']
}

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
    _eq?: Maybe<Scalars['String']>
    _gt?: Maybe<Scalars['String']>
    _gte?: Maybe<Scalars['String']>
    _ilike?: Maybe<Scalars['String']>
    _in?: Maybe<Array<Scalars['String']>>
    _is_null?: Maybe<Scalars['Boolean']>
    _like?: Maybe<Scalars['String']>
    _lt?: Maybe<Scalars['String']>
    _lte?: Maybe<Scalars['String']>
    _neq?: Maybe<Scalars['String']>
    _nilike?: Maybe<Scalars['String']>
    _nin?: Maybe<Array<Scalars['String']>>
    _nlike?: Maybe<Scalars['String']>
    _nsimilar?: Maybe<Scalars['String']>
    _similar?: Maybe<Scalars['String']>
}

/**
 * Discord server channels
 *
 *
 * columns and relationships of "channel"
 */
export type Channel = {
    __typename?: 'channel'
    channel_created_at: Scalars['timestamptz']
    created_at?: Maybe<Scalars['timestamptz']>
    /** An object relationship */
    guild: Guild
    guild_id: Scalars['String']
    history_complete: Scalars['Boolean']
    id: Scalars['String']
    manageable: Scalars['Boolean']
    /** An array relationship */
    messages: Array<Message>
    /** An aggregated array relationship */
    messages_aggregate: Message_Aggregate
    name: Scalars['String']
    nsfw: Scalars['Boolean']
    position: Scalars['numeric']
    rawPosition: Scalars['numeric']
    topic?: Maybe<Scalars['String']>
    type: Scalars['String']
    updated_at?: Maybe<Scalars['timestamptz']>
    viewable: Scalars['Boolean']
}

/**
 * Discord server channels
 *
 *
 * columns and relationships of "channel"
 */
export type ChannelMessagesArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/**
 * Discord server channels
 *
 *
 * columns and relationships of "channel"
 */
export type ChannelMessages_AggregateArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** aggregated selection of "channel" */
export type Channel_Aggregate = {
    __typename?: 'channel_aggregate'
    aggregate?: Maybe<Channel_Aggregate_Fields>
    nodes: Array<Channel>
}

/** aggregate fields of "channel" */
export type Channel_Aggregate_Fields = {
    __typename?: 'channel_aggregate_fields'
    avg?: Maybe<Channel_Avg_Fields>
    count?: Maybe<Scalars['Int']>
    max?: Maybe<Channel_Max_Fields>
    min?: Maybe<Channel_Min_Fields>
    stddev?: Maybe<Channel_Stddev_Fields>
    stddev_pop?: Maybe<Channel_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Channel_Stddev_Samp_Fields>
    sum?: Maybe<Channel_Sum_Fields>
    var_pop?: Maybe<Channel_Var_Pop_Fields>
    var_samp?: Maybe<Channel_Var_Samp_Fields>
    variance?: Maybe<Channel_Variance_Fields>
}

/** aggregate fields of "channel" */
export type Channel_Aggregate_FieldsCountArgs = {
    columns?: Maybe<Array<Channel_Select_Column>>
    distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "channel" */
export type Channel_Aggregate_Order_By = {
    avg?: Maybe<Channel_Avg_Order_By>
    count?: Maybe<Order_By>
    max?: Maybe<Channel_Max_Order_By>
    min?: Maybe<Channel_Min_Order_By>
    stddev?: Maybe<Channel_Stddev_Order_By>
    stddev_pop?: Maybe<Channel_Stddev_Pop_Order_By>
    stddev_samp?: Maybe<Channel_Stddev_Samp_Order_By>
    sum?: Maybe<Channel_Sum_Order_By>
    var_pop?: Maybe<Channel_Var_Pop_Order_By>
    var_samp?: Maybe<Channel_Var_Samp_Order_By>
    variance?: Maybe<Channel_Variance_Order_By>
}

/** input type for inserting array relation for remote table "channel" */
export type Channel_Arr_Rel_Insert_Input = {
    data: Array<Channel_Insert_Input>
    on_conflict?: Maybe<Channel_On_Conflict>
}

/** aggregate avg on columns */
export type Channel_Avg_Fields = {
    __typename?: 'channel_avg_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "channel" */
export type Channel_Avg_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "channel". All fields are combined with a logical 'AND'. */
export type Channel_Bool_Exp = {
    _and?: Maybe<Array<Maybe<Channel_Bool_Exp>>>
    _not?: Maybe<Channel_Bool_Exp>
    _or?: Maybe<Array<Maybe<Channel_Bool_Exp>>>
    channel_created_at?: Maybe<Timestamptz_Comparison_Exp>
    created_at?: Maybe<Timestamptz_Comparison_Exp>
    guild?: Maybe<Guild_Bool_Exp>
    guild_id?: Maybe<String_Comparison_Exp>
    history_complete?: Maybe<Boolean_Comparison_Exp>
    id?: Maybe<String_Comparison_Exp>
    manageable?: Maybe<Boolean_Comparison_Exp>
    messages?: Maybe<Message_Bool_Exp>
    name?: Maybe<String_Comparison_Exp>
    nsfw?: Maybe<Boolean_Comparison_Exp>
    position?: Maybe<Numeric_Comparison_Exp>
    rawPosition?: Maybe<Numeric_Comparison_Exp>
    topic?: Maybe<String_Comparison_Exp>
    type?: Maybe<String_Comparison_Exp>
    updated_at?: Maybe<Timestamptz_Comparison_Exp>
    viewable?: Maybe<Boolean_Comparison_Exp>
}

/** unique or primary key constraints on table "channel" */
export enum Channel_Constraint {
    /** unique or primary key constraint */
    ChannelPkey = 'channel_pkey',
}

/** input type for incrementing integer column in table "channel" */
export type Channel_Inc_Input = {
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "channel" */
export type Channel_Insert_Input = {
    channel_created_at?: Maybe<Scalars['timestamptz']>
    created_at?: Maybe<Scalars['timestamptz']>
    guild?: Maybe<Guild_Obj_Rel_Insert_Input>
    guild_id?: Maybe<Scalars['String']>
    history_complete?: Maybe<Scalars['Boolean']>
    id?: Maybe<Scalars['String']>
    manageable?: Maybe<Scalars['Boolean']>
    messages?: Maybe<Message_Arr_Rel_Insert_Input>
    name?: Maybe<Scalars['String']>
    nsfw?: Maybe<Scalars['Boolean']>
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
    topic?: Maybe<Scalars['String']>
    type?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    viewable?: Maybe<Scalars['Boolean']>
}

/** aggregate max on columns */
export type Channel_Max_Fields = {
    __typename?: 'channel_max_fields'
    channel_created_at?: Maybe<Scalars['timestamptz']>
    created_at?: Maybe<Scalars['timestamptz']>
    guild_id?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    name?: Maybe<Scalars['String']>
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
    topic?: Maybe<Scalars['String']>
    type?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "channel" */
export type Channel_Max_Order_By = {
    channel_created_at?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    guild_id?: Maybe<Order_By>
    id?: Maybe<Order_By>
    name?: Maybe<Order_By>
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
    topic?: Maybe<Order_By>
    type?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Channel_Min_Fields = {
    __typename?: 'channel_min_fields'
    channel_created_at?: Maybe<Scalars['timestamptz']>
    created_at?: Maybe<Scalars['timestamptz']>
    guild_id?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    name?: Maybe<Scalars['String']>
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
    topic?: Maybe<Scalars['String']>
    type?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "channel" */
export type Channel_Min_Order_By = {
    channel_created_at?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    guild_id?: Maybe<Order_By>
    id?: Maybe<Order_By>
    name?: Maybe<Order_By>
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
    topic?: Maybe<Order_By>
    type?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "channel" */
export type Channel_Mutation_Response = {
    __typename?: 'channel_mutation_response'
    /** number of affected rows by the mutation */
    affected_rows: Scalars['Int']
    /** data of the affected rows by the mutation */
    returning: Array<Channel>
}

/** input type for inserting object relation for remote table "channel" */
export type Channel_Obj_Rel_Insert_Input = {
    data: Channel_Insert_Input
    on_conflict?: Maybe<Channel_On_Conflict>
}

/** on conflict condition type for table "channel" */
export type Channel_On_Conflict = {
    constraint: Channel_Constraint
    update_columns: Array<Channel_Update_Column>
    where?: Maybe<Channel_Bool_Exp>
}

/** ordering options when selecting data from "channel" */
export type Channel_Order_By = {
    channel_created_at?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    guild?: Maybe<Guild_Order_By>
    guild_id?: Maybe<Order_By>
    history_complete?: Maybe<Order_By>
    id?: Maybe<Order_By>
    manageable?: Maybe<Order_By>
    messages_aggregate?: Maybe<Message_Aggregate_Order_By>
    name?: Maybe<Order_By>
    nsfw?: Maybe<Order_By>
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
    topic?: Maybe<Order_By>
    type?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
    viewable?: Maybe<Order_By>
}

/** primary key columns input for table: "channel" */
export type Channel_Pk_Columns_Input = {
    id: Scalars['String']
}

/** select columns of table "channel" */
export enum Channel_Select_Column {
    /** column name */
    ChannelCreatedAt = 'channel_created_at',
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    GuildId = 'guild_id',
    /** column name */
    HistoryComplete = 'history_complete',
    /** column name */
    Id = 'id',
    /** column name */
    Manageable = 'manageable',
    /** column name */
    Name = 'name',
    /** column name */
    Nsfw = 'nsfw',
    /** column name */
    Position = 'position',
    /** column name */
    RawPosition = 'rawPosition',
    /** column name */
    Topic = 'topic',
    /** column name */
    Type = 'type',
    /** column name */
    UpdatedAt = 'updated_at',
    /** column name */
    Viewable = 'viewable',
}

/** input type for updating data in table "channel" */
export type Channel_Set_Input = {
    channel_created_at?: Maybe<Scalars['timestamptz']>
    created_at?: Maybe<Scalars['timestamptz']>
    guild_id?: Maybe<Scalars['String']>
    history_complete?: Maybe<Scalars['Boolean']>
    id?: Maybe<Scalars['String']>
    manageable?: Maybe<Scalars['Boolean']>
    name?: Maybe<Scalars['String']>
    nsfw?: Maybe<Scalars['Boolean']>
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
    topic?: Maybe<Scalars['String']>
    type?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    viewable?: Maybe<Scalars['Boolean']>
}

/** aggregate stddev on columns */
export type Channel_Stddev_Fields = {
    __typename?: 'channel_stddev_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "channel" */
export type Channel_Stddev_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Channel_Stddev_Pop_Fields = {
    __typename?: 'channel_stddev_pop_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "channel" */
export type Channel_Stddev_Pop_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Channel_Stddev_Samp_Fields = {
    __typename?: 'channel_stddev_samp_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "channel" */
export type Channel_Stddev_Samp_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Channel_Sum_Fields = {
    __typename?: 'channel_sum_fields'
    position?: Maybe<Scalars['numeric']>
    rawPosition?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "channel" */
export type Channel_Sum_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** update columns of table "channel" */
export enum Channel_Update_Column {
    /** column name */
    ChannelCreatedAt = 'channel_created_at',
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    GuildId = 'guild_id',
    /** column name */
    HistoryComplete = 'history_complete',
    /** column name */
    Id = 'id',
    /** column name */
    Manageable = 'manageable',
    /** column name */
    Name = 'name',
    /** column name */
    Nsfw = 'nsfw',
    /** column name */
    Position = 'position',
    /** column name */
    RawPosition = 'rawPosition',
    /** column name */
    Topic = 'topic',
    /** column name */
    Type = 'type',
    /** column name */
    UpdatedAt = 'updated_at',
    /** column name */
    Viewable = 'viewable',
}

/** aggregate var_pop on columns */
export type Channel_Var_Pop_Fields = {
    __typename?: 'channel_var_pop_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "channel" */
export type Channel_Var_Pop_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Channel_Var_Samp_Fields = {
    __typename?: 'channel_var_samp_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "channel" */
export type Channel_Var_Samp_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Channel_Variance_Fields = {
    __typename?: 'channel_variance_fields'
    position?: Maybe<Scalars['Float']>
    rawPosition?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "channel" */
export type Channel_Variance_Order_By = {
    position?: Maybe<Order_By>
    rawPosition?: Maybe<Order_By>
}

/**
 * Joined discord guilds
 *
 *
 * columns and relationships of "guild"
 */
export type Guild = {
    __typename?: 'guild'
    /** An array relationship */
    channels: Array<Channel>
    /** An aggregated array relationship */
    channels_aggregate: Channel_Aggregate
    created_at: Scalars['timestamptz']
    guild_created_at: Scalars['timestamptz']
    id: Scalars['String']
    joined_at: Scalars['timestamptz']
    memberCount: Scalars['numeric']
    /** An array relationship */
    members: Array<Member>
    /** An aggregated array relationship */
    members_aggregate: Member_Aggregate
    mfaLevel: Scalars['numeric']
    name: Scalars['String']
    owner_id: Scalars['String']
    region: Scalars['String']
    updated_at: Scalars['timestamptz']
}

/**
 * Joined discord guilds
 *
 *
 * columns and relationships of "guild"
 */
export type GuildChannelsArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/**
 * Joined discord guilds
 *
 *
 * columns and relationships of "guild"
 */
export type GuildChannels_AggregateArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/**
 * Joined discord guilds
 *
 *
 * columns and relationships of "guild"
 */
export type GuildMembersArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/**
 * Joined discord guilds
 *
 *
 * columns and relationships of "guild"
 */
export type GuildMembers_AggregateArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** aggregated selection of "guild" */
export type Guild_Aggregate = {
    __typename?: 'guild_aggregate'
    aggregate?: Maybe<Guild_Aggregate_Fields>
    nodes: Array<Guild>
}

/** aggregate fields of "guild" */
export type Guild_Aggregate_Fields = {
    __typename?: 'guild_aggregate_fields'
    avg?: Maybe<Guild_Avg_Fields>
    count?: Maybe<Scalars['Int']>
    max?: Maybe<Guild_Max_Fields>
    min?: Maybe<Guild_Min_Fields>
    stddev?: Maybe<Guild_Stddev_Fields>
    stddev_pop?: Maybe<Guild_Stddev_Pop_Fields>
    stddev_samp?: Maybe<Guild_Stddev_Samp_Fields>
    sum?: Maybe<Guild_Sum_Fields>
    var_pop?: Maybe<Guild_Var_Pop_Fields>
    var_samp?: Maybe<Guild_Var_Samp_Fields>
    variance?: Maybe<Guild_Variance_Fields>
}

/** aggregate fields of "guild" */
export type Guild_Aggregate_FieldsCountArgs = {
    columns?: Maybe<Array<Guild_Select_Column>>
    distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "guild" */
export type Guild_Aggregate_Order_By = {
    avg?: Maybe<Guild_Avg_Order_By>
    count?: Maybe<Order_By>
    max?: Maybe<Guild_Max_Order_By>
    min?: Maybe<Guild_Min_Order_By>
    stddev?: Maybe<Guild_Stddev_Order_By>
    stddev_pop?: Maybe<Guild_Stddev_Pop_Order_By>
    stddev_samp?: Maybe<Guild_Stddev_Samp_Order_By>
    sum?: Maybe<Guild_Sum_Order_By>
    var_pop?: Maybe<Guild_Var_Pop_Order_By>
    var_samp?: Maybe<Guild_Var_Samp_Order_By>
    variance?: Maybe<Guild_Variance_Order_By>
}

/** input type for inserting array relation for remote table "guild" */
export type Guild_Arr_Rel_Insert_Input = {
    data: Array<Guild_Insert_Input>
    on_conflict?: Maybe<Guild_On_Conflict>
}

/** aggregate avg on columns */
export type Guild_Avg_Fields = {
    __typename?: 'guild_avg_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "guild" */
export type Guild_Avg_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "guild". All fields are combined with a logical 'AND'. */
export type Guild_Bool_Exp = {
    _and?: Maybe<Array<Maybe<Guild_Bool_Exp>>>
    _not?: Maybe<Guild_Bool_Exp>
    _or?: Maybe<Array<Maybe<Guild_Bool_Exp>>>
    channels?: Maybe<Channel_Bool_Exp>
    created_at?: Maybe<Timestamptz_Comparison_Exp>
    guild_created_at?: Maybe<Timestamptz_Comparison_Exp>
    id?: Maybe<String_Comparison_Exp>
    joined_at?: Maybe<Timestamptz_Comparison_Exp>
    memberCount?: Maybe<Numeric_Comparison_Exp>
    members?: Maybe<Member_Bool_Exp>
    mfaLevel?: Maybe<Numeric_Comparison_Exp>
    name?: Maybe<String_Comparison_Exp>
    owner_id?: Maybe<String_Comparison_Exp>
    region?: Maybe<String_Comparison_Exp>
    updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "guild" */
export enum Guild_Constraint {
    /** unique or primary key constraint */
    GuildPkey = 'guild_pkey',
}

/** input type for incrementing integer column in table "guild" */
export type Guild_Inc_Input = {
    memberCount?: Maybe<Scalars['numeric']>
    mfaLevel?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "guild" */
export type Guild_Insert_Input = {
    channels?: Maybe<Channel_Arr_Rel_Insert_Input>
    created_at?: Maybe<Scalars['timestamptz']>
    guild_created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    joined_at?: Maybe<Scalars['timestamptz']>
    memberCount?: Maybe<Scalars['numeric']>
    members?: Maybe<Member_Arr_Rel_Insert_Input>
    mfaLevel?: Maybe<Scalars['numeric']>
    name?: Maybe<Scalars['String']>
    owner_id?: Maybe<Scalars['String']>
    region?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Guild_Max_Fields = {
    __typename?: 'guild_max_fields'
    created_at?: Maybe<Scalars['timestamptz']>
    guild_created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    joined_at?: Maybe<Scalars['timestamptz']>
    memberCount?: Maybe<Scalars['numeric']>
    mfaLevel?: Maybe<Scalars['numeric']>
    name?: Maybe<Scalars['String']>
    owner_id?: Maybe<Scalars['String']>
    region?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "guild" */
export type Guild_Max_Order_By = {
    created_at?: Maybe<Order_By>
    guild_created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    joined_at?: Maybe<Order_By>
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
    name?: Maybe<Order_By>
    owner_id?: Maybe<Order_By>
    region?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Guild_Min_Fields = {
    __typename?: 'guild_min_fields'
    created_at?: Maybe<Scalars['timestamptz']>
    guild_created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    joined_at?: Maybe<Scalars['timestamptz']>
    memberCount?: Maybe<Scalars['numeric']>
    mfaLevel?: Maybe<Scalars['numeric']>
    name?: Maybe<Scalars['String']>
    owner_id?: Maybe<Scalars['String']>
    region?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "guild" */
export type Guild_Min_Order_By = {
    created_at?: Maybe<Order_By>
    guild_created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    joined_at?: Maybe<Order_By>
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
    name?: Maybe<Order_By>
    owner_id?: Maybe<Order_By>
    region?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "guild" */
export type Guild_Mutation_Response = {
    __typename?: 'guild_mutation_response'
    /** number of affected rows by the mutation */
    affected_rows: Scalars['Int']
    /** data of the affected rows by the mutation */
    returning: Array<Guild>
}

/** input type for inserting object relation for remote table "guild" */
export type Guild_Obj_Rel_Insert_Input = {
    data: Guild_Insert_Input
    on_conflict?: Maybe<Guild_On_Conflict>
}

/** on conflict condition type for table "guild" */
export type Guild_On_Conflict = {
    constraint: Guild_Constraint
    update_columns: Array<Guild_Update_Column>
    where?: Maybe<Guild_Bool_Exp>
}

/** ordering options when selecting data from "guild" */
export type Guild_Order_By = {
    channels_aggregate?: Maybe<Channel_Aggregate_Order_By>
    created_at?: Maybe<Order_By>
    guild_created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    joined_at?: Maybe<Order_By>
    memberCount?: Maybe<Order_By>
    members_aggregate?: Maybe<Member_Aggregate_Order_By>
    mfaLevel?: Maybe<Order_By>
    name?: Maybe<Order_By>
    owner_id?: Maybe<Order_By>
    region?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "guild" */
export type Guild_Pk_Columns_Input = {
    id: Scalars['String']
}

/** select columns of table "guild" */
export enum Guild_Select_Column {
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    GuildCreatedAt = 'guild_created_at',
    /** column name */
    Id = 'id',
    /** column name */
    JoinedAt = 'joined_at',
    /** column name */
    MemberCount = 'memberCount',
    /** column name */
    MfaLevel = 'mfaLevel',
    /** column name */
    Name = 'name',
    /** column name */
    OwnerId = 'owner_id',
    /** column name */
    Region = 'region',
    /** column name */
    UpdatedAt = 'updated_at',
}

/** input type for updating data in table "guild" */
export type Guild_Set_Input = {
    created_at?: Maybe<Scalars['timestamptz']>
    guild_created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    joined_at?: Maybe<Scalars['timestamptz']>
    memberCount?: Maybe<Scalars['numeric']>
    mfaLevel?: Maybe<Scalars['numeric']>
    name?: Maybe<Scalars['String']>
    owner_id?: Maybe<Scalars['String']>
    region?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Guild_Stddev_Fields = {
    __typename?: 'guild_stddev_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "guild" */
export type Guild_Stddev_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Guild_Stddev_Pop_Fields = {
    __typename?: 'guild_stddev_pop_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "guild" */
export type Guild_Stddev_Pop_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Guild_Stddev_Samp_Fields = {
    __typename?: 'guild_stddev_samp_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "guild" */
export type Guild_Stddev_Samp_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Guild_Sum_Fields = {
    __typename?: 'guild_sum_fields'
    memberCount?: Maybe<Scalars['numeric']>
    mfaLevel?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "guild" */
export type Guild_Sum_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** update columns of table "guild" */
export enum Guild_Update_Column {
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    GuildCreatedAt = 'guild_created_at',
    /** column name */
    Id = 'id',
    /** column name */
    JoinedAt = 'joined_at',
    /** column name */
    MemberCount = 'memberCount',
    /** column name */
    MfaLevel = 'mfaLevel',
    /** column name */
    Name = 'name',
    /** column name */
    OwnerId = 'owner_id',
    /** column name */
    Region = 'region',
    /** column name */
    UpdatedAt = 'updated_at',
}

/** aggregate var_pop on columns */
export type Guild_Var_Pop_Fields = {
    __typename?: 'guild_var_pop_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "guild" */
export type Guild_Var_Pop_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Guild_Var_Samp_Fields = {
    __typename?: 'guild_var_samp_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "guild" */
export type Guild_Var_Samp_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Guild_Variance_Fields = {
    __typename?: 'guild_variance_fields'
    memberCount?: Maybe<Scalars['Float']>
    mfaLevel?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "guild" */
export type Guild_Variance_Order_By = {
    memberCount?: Maybe<Order_By>
    mfaLevel?: Maybe<Order_By>
}

/**
 * Discord guild members
 *
 *
 * columns and relationships of "member"
 */
export type Member = {
    __typename?: 'member'
    bannable: Scalars['Boolean']
    display_name?: Maybe<Scalars['String']>
    /** An object relationship */
    guild: Guild
    guild_id: Scalars['String']
    kickable: Scalars['Boolean']
    manageable: Scalars['Boolean']
    member_joined_at: Scalars['timestamptz']
    nickname?: Maybe<Scalars['String']>
    /** An object relationship */
    user: User
    user_id: Scalars['String']
}

/** aggregated selection of "member" */
export type Member_Aggregate = {
    __typename?: 'member_aggregate'
    aggregate?: Maybe<Member_Aggregate_Fields>
    nodes: Array<Member>
}

/** aggregate fields of "member" */
export type Member_Aggregate_Fields = {
    __typename?: 'member_aggregate_fields'
    count?: Maybe<Scalars['Int']>
    max?: Maybe<Member_Max_Fields>
    min?: Maybe<Member_Min_Fields>
}

/** aggregate fields of "member" */
export type Member_Aggregate_FieldsCountArgs = {
    columns?: Maybe<Array<Member_Select_Column>>
    distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "member" */
export type Member_Aggregate_Order_By = {
    count?: Maybe<Order_By>
    max?: Maybe<Member_Max_Order_By>
    min?: Maybe<Member_Min_Order_By>
}

/** input type for inserting array relation for remote table "member" */
export type Member_Arr_Rel_Insert_Input = {
    data: Array<Member_Insert_Input>
    on_conflict?: Maybe<Member_On_Conflict>
}

/** Boolean expression to filter rows from the table "member". All fields are combined with a logical 'AND'. */
export type Member_Bool_Exp = {
    _and?: Maybe<Array<Maybe<Member_Bool_Exp>>>
    _not?: Maybe<Member_Bool_Exp>
    _or?: Maybe<Array<Maybe<Member_Bool_Exp>>>
    bannable?: Maybe<Boolean_Comparison_Exp>
    display_name?: Maybe<String_Comparison_Exp>
    guild?: Maybe<Guild_Bool_Exp>
    guild_id?: Maybe<String_Comparison_Exp>
    kickable?: Maybe<Boolean_Comparison_Exp>
    manageable?: Maybe<Boolean_Comparison_Exp>
    member_joined_at?: Maybe<Timestamptz_Comparison_Exp>
    nickname?: Maybe<String_Comparison_Exp>
    user?: Maybe<User_Bool_Exp>
    user_id?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "member" */
export enum Member_Constraint {
    /** unique or primary key constraint */
    MemberPkey = 'member_pkey',
}

/** input type for inserting data into table "member" */
export type Member_Insert_Input = {
    bannable?: Maybe<Scalars['Boolean']>
    display_name?: Maybe<Scalars['String']>
    guild?: Maybe<Guild_Obj_Rel_Insert_Input>
    guild_id?: Maybe<Scalars['String']>
    kickable?: Maybe<Scalars['Boolean']>
    manageable?: Maybe<Scalars['Boolean']>
    member_joined_at?: Maybe<Scalars['timestamptz']>
    nickname?: Maybe<Scalars['String']>
    user?: Maybe<User_Obj_Rel_Insert_Input>
    user_id?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Member_Max_Fields = {
    __typename?: 'member_max_fields'
    display_name?: Maybe<Scalars['String']>
    guild_id?: Maybe<Scalars['String']>
    member_joined_at?: Maybe<Scalars['timestamptz']>
    nickname?: Maybe<Scalars['String']>
    user_id?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "member" */
export type Member_Max_Order_By = {
    display_name?: Maybe<Order_By>
    guild_id?: Maybe<Order_By>
    member_joined_at?: Maybe<Order_By>
    nickname?: Maybe<Order_By>
    user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Member_Min_Fields = {
    __typename?: 'member_min_fields'
    display_name?: Maybe<Scalars['String']>
    guild_id?: Maybe<Scalars['String']>
    member_joined_at?: Maybe<Scalars['timestamptz']>
    nickname?: Maybe<Scalars['String']>
    user_id?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "member" */
export type Member_Min_Order_By = {
    display_name?: Maybe<Order_By>
    guild_id?: Maybe<Order_By>
    member_joined_at?: Maybe<Order_By>
    nickname?: Maybe<Order_By>
    user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "member" */
export type Member_Mutation_Response = {
    __typename?: 'member_mutation_response'
    /** number of affected rows by the mutation */
    affected_rows: Scalars['Int']
    /** data of the affected rows by the mutation */
    returning: Array<Member>
}

/** input type for inserting object relation for remote table "member" */
export type Member_Obj_Rel_Insert_Input = {
    data: Member_Insert_Input
    on_conflict?: Maybe<Member_On_Conflict>
}

/** on conflict condition type for table "member" */
export type Member_On_Conflict = {
    constraint: Member_Constraint
    update_columns: Array<Member_Update_Column>
    where?: Maybe<Member_Bool_Exp>
}

/** ordering options when selecting data from "member" */
export type Member_Order_By = {
    bannable?: Maybe<Order_By>
    display_name?: Maybe<Order_By>
    guild?: Maybe<Guild_Order_By>
    guild_id?: Maybe<Order_By>
    kickable?: Maybe<Order_By>
    manageable?: Maybe<Order_By>
    member_joined_at?: Maybe<Order_By>
    nickname?: Maybe<Order_By>
    user?: Maybe<User_Order_By>
    user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "member" */
export type Member_Pk_Columns_Input = {
    guild_id: Scalars['String']
    user_id: Scalars['String']
}

/** select columns of table "member" */
export enum Member_Select_Column {
    /** column name */
    Bannable = 'bannable',
    /** column name */
    DisplayName = 'display_name',
    /** column name */
    GuildId = 'guild_id',
    /** column name */
    Kickable = 'kickable',
    /** column name */
    Manageable = 'manageable',
    /** column name */
    MemberJoinedAt = 'member_joined_at',
    /** column name */
    Nickname = 'nickname',
    /** column name */
    UserId = 'user_id',
}

/** input type for updating data in table "member" */
export type Member_Set_Input = {
    bannable?: Maybe<Scalars['Boolean']>
    display_name?: Maybe<Scalars['String']>
    guild_id?: Maybe<Scalars['String']>
    kickable?: Maybe<Scalars['Boolean']>
    manageable?: Maybe<Scalars['Boolean']>
    member_joined_at?: Maybe<Scalars['timestamptz']>
    nickname?: Maybe<Scalars['String']>
    user_id?: Maybe<Scalars['String']>
}

/** update columns of table "member" */
export enum Member_Update_Column {
    /** column name */
    Bannable = 'bannable',
    /** column name */
    DisplayName = 'display_name',
    /** column name */
    GuildId = 'guild_id',
    /** column name */
    Kickable = 'kickable',
    /** column name */
    Manageable = 'manageable',
    /** column name */
    MemberJoinedAt = 'member_joined_at',
    /** column name */
    Nickname = 'nickname',
    /** column name */
    UserId = 'user_id',
}

/**
 * Discord messages
 *
 *
 * columns and relationships of "message"
 */
export type Message = {
    __typename?: 'message'
    /** An object relationship */
    channel: Channel
    channel_id: Scalars['String']
    cleanContent: Scalars['String']
    content: Scalars['String']
    id: Scalars['String']
    message_created_at: Scalars['timestamptz']
    tts: Scalars['Boolean']
    /** An object relationship */
    user?: Maybe<User>
    user_id?: Maybe<Scalars['String']>
}

/** aggregated selection of "message" */
export type Message_Aggregate = {
    __typename?: 'message_aggregate'
    aggregate?: Maybe<Message_Aggregate_Fields>
    nodes: Array<Message>
}

/** aggregate fields of "message" */
export type Message_Aggregate_Fields = {
    __typename?: 'message_aggregate_fields'
    count?: Maybe<Scalars['Int']>
    max?: Maybe<Message_Max_Fields>
    min?: Maybe<Message_Min_Fields>
}

/** aggregate fields of "message" */
export type Message_Aggregate_FieldsCountArgs = {
    columns?: Maybe<Array<Message_Select_Column>>
    distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "message" */
export type Message_Aggregate_Order_By = {
    count?: Maybe<Order_By>
    max?: Maybe<Message_Max_Order_By>
    min?: Maybe<Message_Min_Order_By>
}

/** input type for inserting array relation for remote table "message" */
export type Message_Arr_Rel_Insert_Input = {
    data: Array<Message_Insert_Input>
    on_conflict?: Maybe<Message_On_Conflict>
}

/** Boolean expression to filter rows from the table "message". All fields are combined with a logical 'AND'. */
export type Message_Bool_Exp = {
    _and?: Maybe<Array<Maybe<Message_Bool_Exp>>>
    _not?: Maybe<Message_Bool_Exp>
    _or?: Maybe<Array<Maybe<Message_Bool_Exp>>>
    channel?: Maybe<Channel_Bool_Exp>
    channel_id?: Maybe<String_Comparison_Exp>
    cleanContent?: Maybe<String_Comparison_Exp>
    content?: Maybe<String_Comparison_Exp>
    id?: Maybe<String_Comparison_Exp>
    message_created_at?: Maybe<Timestamptz_Comparison_Exp>
    tts?: Maybe<Boolean_Comparison_Exp>
    user?: Maybe<User_Bool_Exp>
    user_id?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "message" */
export enum Message_Constraint {
    /** unique or primary key constraint */
    MessagePkey = 'message_pkey',
}

/** input type for inserting data into table "message" */
export type Message_Insert_Input = {
    channel?: Maybe<Channel_Obj_Rel_Insert_Input>
    channel_id?: Maybe<Scalars['String']>
    cleanContent?: Maybe<Scalars['String']>
    content?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    message_created_at?: Maybe<Scalars['timestamptz']>
    tts?: Maybe<Scalars['Boolean']>
    user?: Maybe<User_Obj_Rel_Insert_Input>
    user_id?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Message_Max_Fields = {
    __typename?: 'message_max_fields'
    channel_id?: Maybe<Scalars['String']>
    cleanContent?: Maybe<Scalars['String']>
    content?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    message_created_at?: Maybe<Scalars['timestamptz']>
    user_id?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "message" */
export type Message_Max_Order_By = {
    channel_id?: Maybe<Order_By>
    cleanContent?: Maybe<Order_By>
    content?: Maybe<Order_By>
    id?: Maybe<Order_By>
    message_created_at?: Maybe<Order_By>
    user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Message_Min_Fields = {
    __typename?: 'message_min_fields'
    channel_id?: Maybe<Scalars['String']>
    cleanContent?: Maybe<Scalars['String']>
    content?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    message_created_at?: Maybe<Scalars['timestamptz']>
    user_id?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "message" */
export type Message_Min_Order_By = {
    channel_id?: Maybe<Order_By>
    cleanContent?: Maybe<Order_By>
    content?: Maybe<Order_By>
    id?: Maybe<Order_By>
    message_created_at?: Maybe<Order_By>
    user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "message" */
export type Message_Mutation_Response = {
    __typename?: 'message_mutation_response'
    /** number of affected rows by the mutation */
    affected_rows: Scalars['Int']
    /** data of the affected rows by the mutation */
    returning: Array<Message>
}

/** input type for inserting object relation for remote table "message" */
export type Message_Obj_Rel_Insert_Input = {
    data: Message_Insert_Input
    on_conflict?: Maybe<Message_On_Conflict>
}

/** on conflict condition type for table "message" */
export type Message_On_Conflict = {
    constraint: Message_Constraint
    update_columns: Array<Message_Update_Column>
    where?: Maybe<Message_Bool_Exp>
}

/** ordering options when selecting data from "message" */
export type Message_Order_By = {
    channel?: Maybe<Channel_Order_By>
    channel_id?: Maybe<Order_By>
    cleanContent?: Maybe<Order_By>
    content?: Maybe<Order_By>
    id?: Maybe<Order_By>
    message_created_at?: Maybe<Order_By>
    tts?: Maybe<Order_By>
    user?: Maybe<User_Order_By>
    user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "message" */
export type Message_Pk_Columns_Input = {
    id: Scalars['String']
}

/** select columns of table "message" */
export enum Message_Select_Column {
    /** column name */
    ChannelId = 'channel_id',
    /** column name */
    CleanContent = 'cleanContent',
    /** column name */
    Content = 'content',
    /** column name */
    Id = 'id',
    /** column name */
    MessageCreatedAt = 'message_created_at',
    /** column name */
    Tts = 'tts',
    /** column name */
    UserId = 'user_id',
}

/** input type for updating data in table "message" */
export type Message_Set_Input = {
    channel_id?: Maybe<Scalars['String']>
    cleanContent?: Maybe<Scalars['String']>
    content?: Maybe<Scalars['String']>
    id?: Maybe<Scalars['String']>
    message_created_at?: Maybe<Scalars['timestamptz']>
    tts?: Maybe<Scalars['Boolean']>
    user_id?: Maybe<Scalars['String']>
}

/** update columns of table "message" */
export enum Message_Update_Column {
    /** column name */
    ChannelId = 'channel_id',
    /** column name */
    CleanContent = 'cleanContent',
    /** column name */
    Content = 'content',
    /** column name */
    Id = 'id',
    /** column name */
    MessageCreatedAt = 'message_created_at',
    /** column name */
    Tts = 'tts',
    /** column name */
    UserId = 'user_id',
}

/** mutation root */
export type Mutation_Root = {
    __typename?: 'mutation_root'
    /** delete data from the table: "channel" */
    delete_channel?: Maybe<Channel_Mutation_Response>
    /** delete single row from the table: "channel" */
    delete_channel_by_pk?: Maybe<Channel>
    /** delete data from the table: "guild" */
    delete_guild?: Maybe<Guild_Mutation_Response>
    /** delete single row from the table: "guild" */
    delete_guild_by_pk?: Maybe<Guild>
    /** delete data from the table: "member" */
    delete_member?: Maybe<Member_Mutation_Response>
    /** delete single row from the table: "member" */
    delete_member_by_pk?: Maybe<Member>
    /** delete data from the table: "message" */
    delete_message?: Maybe<Message_Mutation_Response>
    /** delete single row from the table: "message" */
    delete_message_by_pk?: Maybe<Message>
    /** delete data from the table: "user" */
    delete_user?: Maybe<User_Mutation_Response>
    /** delete single row from the table: "user" */
    delete_user_by_pk?: Maybe<User>
    /** insert data into the table: "channel" */
    insert_channel?: Maybe<Channel_Mutation_Response>
    /** insert a single row into the table: "channel" */
    insert_channel_one?: Maybe<Channel>
    /** insert data into the table: "guild" */
    insert_guild?: Maybe<Guild_Mutation_Response>
    /** insert a single row into the table: "guild" */
    insert_guild_one?: Maybe<Guild>
    /** insert data into the table: "member" */
    insert_member?: Maybe<Member_Mutation_Response>
    /** insert a single row into the table: "member" */
    insert_member_one?: Maybe<Member>
    /** insert data into the table: "message" */
    insert_message?: Maybe<Message_Mutation_Response>
    /** insert a single row into the table: "message" */
    insert_message_one?: Maybe<Message>
    /** insert data into the table: "user" */
    insert_user?: Maybe<User_Mutation_Response>
    /** insert a single row into the table: "user" */
    insert_user_one?: Maybe<User>
    /** update data of the table: "channel" */
    update_channel?: Maybe<Channel_Mutation_Response>
    /** update single row of the table: "channel" */
    update_channel_by_pk?: Maybe<Channel>
    /** update data of the table: "guild" */
    update_guild?: Maybe<Guild_Mutation_Response>
    /** update single row of the table: "guild" */
    update_guild_by_pk?: Maybe<Guild>
    /** update data of the table: "member" */
    update_member?: Maybe<Member_Mutation_Response>
    /** update single row of the table: "member" */
    update_member_by_pk?: Maybe<Member>
    /** update data of the table: "message" */
    update_message?: Maybe<Message_Mutation_Response>
    /** update single row of the table: "message" */
    update_message_by_pk?: Maybe<Message>
    /** update data of the table: "user" */
    update_user?: Maybe<User_Mutation_Response>
    /** update single row of the table: "user" */
    update_user_by_pk?: Maybe<User>
}

/** mutation root */
export type Mutation_RootDelete_ChannelArgs = {
    where: Channel_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Channel_By_PkArgs = {
    id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_GuildArgs = {
    where: Guild_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Guild_By_PkArgs = {
    id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_MemberArgs = {
    where: Member_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Member_By_PkArgs = {
    guild_id: Scalars['String']
    user_id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_MessageArgs = {
    where: Message_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Message_By_PkArgs = {
    id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_UserArgs = {
    where: User_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_User_By_PkArgs = {
    id: Scalars['String']
}

/** mutation root */
export type Mutation_RootInsert_ChannelArgs = {
    objects: Array<Channel_Insert_Input>
    on_conflict?: Maybe<Channel_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Channel_OneArgs = {
    object: Channel_Insert_Input
    on_conflict?: Maybe<Channel_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_GuildArgs = {
    objects: Array<Guild_Insert_Input>
    on_conflict?: Maybe<Guild_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Guild_OneArgs = {
    object: Guild_Insert_Input
    on_conflict?: Maybe<Guild_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_MemberArgs = {
    objects: Array<Member_Insert_Input>
    on_conflict?: Maybe<Member_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Member_OneArgs = {
    object: Member_Insert_Input
    on_conflict?: Maybe<Member_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_MessageArgs = {
    objects: Array<Message_Insert_Input>
    on_conflict?: Maybe<Message_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Message_OneArgs = {
    object: Message_Insert_Input
    on_conflict?: Maybe<Message_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_UserArgs = {
    objects: Array<User_Insert_Input>
    on_conflict?: Maybe<User_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_User_OneArgs = {
    object: User_Insert_Input
    on_conflict?: Maybe<User_On_Conflict>
}

/** mutation root */
export type Mutation_RootUpdate_ChannelArgs = {
    _inc?: Maybe<Channel_Inc_Input>
    _set?: Maybe<Channel_Set_Input>
    where: Channel_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Channel_By_PkArgs = {
    _inc?: Maybe<Channel_Inc_Input>
    _set?: Maybe<Channel_Set_Input>
    pk_columns: Channel_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_GuildArgs = {
    _inc?: Maybe<Guild_Inc_Input>
    _set?: Maybe<Guild_Set_Input>
    where: Guild_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Guild_By_PkArgs = {
    _inc?: Maybe<Guild_Inc_Input>
    _set?: Maybe<Guild_Set_Input>
    pk_columns: Guild_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_MemberArgs = {
    _set?: Maybe<Member_Set_Input>
    where: Member_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Member_By_PkArgs = {
    _set?: Maybe<Member_Set_Input>
    pk_columns: Member_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_MessageArgs = {
    _set?: Maybe<Message_Set_Input>
    where: Message_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Message_By_PkArgs = {
    _set?: Maybe<Message_Set_Input>
    pk_columns: Message_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_UserArgs = {
    _set?: Maybe<User_Set_Input>
    where: User_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_User_By_PkArgs = {
    _set?: Maybe<User_Set_Input>
    pk_columns: User_Pk_Columns_Input
}

/** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
    _eq?: Maybe<Scalars['numeric']>
    _gt?: Maybe<Scalars['numeric']>
    _gte?: Maybe<Scalars['numeric']>
    _in?: Maybe<Array<Scalars['numeric']>>
    _is_null?: Maybe<Scalars['Boolean']>
    _lt?: Maybe<Scalars['numeric']>
    _lte?: Maybe<Scalars['numeric']>
    _neq?: Maybe<Scalars['numeric']>
    _nin?: Maybe<Array<Scalars['numeric']>>
}

/** column ordering options */
export enum Order_By {
    /** in the ascending order, nulls last */
    Asc = 'asc',
    /** in the ascending order, nulls first */
    AscNullsFirst = 'asc_nulls_first',
    /** in the ascending order, nulls last */
    AscNullsLast = 'asc_nulls_last',
    /** in the descending order, nulls first */
    Desc = 'desc',
    /** in the descending order, nulls first */
    DescNullsFirst = 'desc_nulls_first',
    /** in the descending order, nulls last */
    DescNullsLast = 'desc_nulls_last',
}

/** query root */
export type Query_Root = {
    __typename?: 'query_root'
    /** fetch data from the table: "channel" */
    channel: Array<Channel>
    /** fetch aggregated fields from the table: "channel" */
    channel_aggregate: Channel_Aggregate
    /** fetch data from the table: "channel" using primary key columns */
    channel_by_pk?: Maybe<Channel>
    /** fetch data from the table: "guild" */
    guild: Array<Guild>
    /** fetch aggregated fields from the table: "guild" */
    guild_aggregate: Guild_Aggregate
    /** fetch data from the table: "guild" using primary key columns */
    guild_by_pk?: Maybe<Guild>
    /** fetch data from the table: "member" */
    member: Array<Member>
    /** fetch aggregated fields from the table: "member" */
    member_aggregate: Member_Aggregate
    /** fetch data from the table: "member" using primary key columns */
    member_by_pk?: Maybe<Member>
    /** fetch data from the table: "message" */
    message: Array<Message>
    /** fetch aggregated fields from the table: "message" */
    message_aggregate: Message_Aggregate
    /** fetch data from the table: "message" using primary key columns */
    message_by_pk?: Maybe<Message>
    /** fetch data from the table: "user" */
    user: Array<User>
    /** fetch aggregated fields from the table: "user" */
    user_aggregate: User_Aggregate
    /** fetch data from the table: "user" using primary key columns */
    user_by_pk?: Maybe<User>
}

/** query root */
export type Query_RootChannelArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/** query root */
export type Query_RootChannel_AggregateArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/** query root */
export type Query_RootChannel_By_PkArgs = {
    id: Scalars['String']
}

/** query root */
export type Query_RootGuildArgs = {
    distinct_on?: Maybe<Array<Guild_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Guild_Order_By>>
    where?: Maybe<Guild_Bool_Exp>
}

/** query root */
export type Query_RootGuild_AggregateArgs = {
    distinct_on?: Maybe<Array<Guild_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Guild_Order_By>>
    where?: Maybe<Guild_Bool_Exp>
}

/** query root */
export type Query_RootGuild_By_PkArgs = {
    id: Scalars['String']
}

/** query root */
export type Query_RootMemberArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** query root */
export type Query_RootMember_AggregateArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** query root */
export type Query_RootMember_By_PkArgs = {
    guild_id: Scalars['String']
    user_id: Scalars['String']
}

/** query root */
export type Query_RootMessageArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** query root */
export type Query_RootMessage_AggregateArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** query root */
export type Query_RootMessage_By_PkArgs = {
    id: Scalars['String']
}

/** query root */
export type Query_RootUserArgs = {
    distinct_on?: Maybe<Array<User_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<User_Order_By>>
    where?: Maybe<User_Bool_Exp>
}

/** query root */
export type Query_RootUser_AggregateArgs = {
    distinct_on?: Maybe<Array<User_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<User_Order_By>>
    where?: Maybe<User_Bool_Exp>
}

/** query root */
export type Query_RootUser_By_PkArgs = {
    id: Scalars['String']
}

/** subscription root */
export type Subscription_Root = {
    __typename?: 'subscription_root'
    /** fetch data from the table: "channel" */
    channel: Array<Channel>
    /** fetch aggregated fields from the table: "channel" */
    channel_aggregate: Channel_Aggregate
    /** fetch data from the table: "channel" using primary key columns */
    channel_by_pk?: Maybe<Channel>
    /** fetch data from the table: "guild" */
    guild: Array<Guild>
    /** fetch aggregated fields from the table: "guild" */
    guild_aggregate: Guild_Aggregate
    /** fetch data from the table: "guild" using primary key columns */
    guild_by_pk?: Maybe<Guild>
    /** fetch data from the table: "member" */
    member: Array<Member>
    /** fetch aggregated fields from the table: "member" */
    member_aggregate: Member_Aggregate
    /** fetch data from the table: "member" using primary key columns */
    member_by_pk?: Maybe<Member>
    /** fetch data from the table: "message" */
    message: Array<Message>
    /** fetch aggregated fields from the table: "message" */
    message_aggregate: Message_Aggregate
    /** fetch data from the table: "message" using primary key columns */
    message_by_pk?: Maybe<Message>
    /** fetch data from the table: "user" */
    user: Array<User>
    /** fetch aggregated fields from the table: "user" */
    user_aggregate: User_Aggregate
    /** fetch data from the table: "user" using primary key columns */
    user_by_pk?: Maybe<User>
}

/** subscription root */
export type Subscription_RootChannelArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/** subscription root */
export type Subscription_RootChannel_AggregateArgs = {
    distinct_on?: Maybe<Array<Channel_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Channel_Order_By>>
    where?: Maybe<Channel_Bool_Exp>
}

/** subscription root */
export type Subscription_RootChannel_By_PkArgs = {
    id: Scalars['String']
}

/** subscription root */
export type Subscription_RootGuildArgs = {
    distinct_on?: Maybe<Array<Guild_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Guild_Order_By>>
    where?: Maybe<Guild_Bool_Exp>
}

/** subscription root */
export type Subscription_RootGuild_AggregateArgs = {
    distinct_on?: Maybe<Array<Guild_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Guild_Order_By>>
    where?: Maybe<Guild_Bool_Exp>
}

/** subscription root */
export type Subscription_RootGuild_By_PkArgs = {
    id: Scalars['String']
}

/** subscription root */
export type Subscription_RootMemberArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMember_AggregateArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMember_By_PkArgs = {
    guild_id: Scalars['String']
    user_id: Scalars['String']
}

/** subscription root */
export type Subscription_RootMessageArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessage_AggregateArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessage_By_PkArgs = {
    id: Scalars['String']
}

/** subscription root */
export type Subscription_RootUserArgs = {
    distinct_on?: Maybe<Array<User_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<User_Order_By>>
    where?: Maybe<User_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUser_AggregateArgs = {
    distinct_on?: Maybe<Array<User_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<User_Order_By>>
    where?: Maybe<User_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUser_By_PkArgs = {
    id: Scalars['String']
}

/** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
    _eq?: Maybe<Scalars['timestamptz']>
    _gt?: Maybe<Scalars['timestamptz']>
    _gte?: Maybe<Scalars['timestamptz']>
    _in?: Maybe<Array<Scalars['timestamptz']>>
    _is_null?: Maybe<Scalars['Boolean']>
    _lt?: Maybe<Scalars['timestamptz']>
    _lte?: Maybe<Scalars['timestamptz']>
    _neq?: Maybe<Scalars['timestamptz']>
    _nin?: Maybe<Array<Scalars['timestamptz']>>
}

/** columns and relationships of "user" */
export type User = {
    __typename?: 'user'
    avatar?: Maybe<Scalars['String']>
    bot: Scalars['Boolean']
    created_at: Scalars['timestamptz']
    id: Scalars['String']
    locale?: Maybe<Scalars['String']>
    /** An array relationship */
    members: Array<Member>
    /** An aggregated array relationship */
    members_aggregate: Member_Aggregate
    /** An array relationship */
    messages: Array<Message>
    /** An aggregated array relationship */
    messages_aggregate: Message_Aggregate
    tag: Scalars['String']
    updated_at: Scalars['timestamptz']
    user_created_at: Scalars['timestamptz']
    username: Scalars['String']
}

/** columns and relationships of "user" */
export type UserMembersArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** columns and relationships of "user" */
export type UserMembers_AggregateArgs = {
    distinct_on?: Maybe<Array<Member_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Member_Order_By>>
    where?: Maybe<Member_Bool_Exp>
}

/** columns and relationships of "user" */
export type UserMessagesArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** columns and relationships of "user" */
export type UserMessages_AggregateArgs = {
    distinct_on?: Maybe<Array<Message_Select_Column>>
    limit?: Maybe<Scalars['Int']>
    offset?: Maybe<Scalars['Int']>
    order_by?: Maybe<Array<Message_Order_By>>
    where?: Maybe<Message_Bool_Exp>
}

/** aggregated selection of "user" */
export type User_Aggregate = {
    __typename?: 'user_aggregate'
    aggregate?: Maybe<User_Aggregate_Fields>
    nodes: Array<User>
}

/** aggregate fields of "user" */
export type User_Aggregate_Fields = {
    __typename?: 'user_aggregate_fields'
    count?: Maybe<Scalars['Int']>
    max?: Maybe<User_Max_Fields>
    min?: Maybe<User_Min_Fields>
}

/** aggregate fields of "user" */
export type User_Aggregate_FieldsCountArgs = {
    columns?: Maybe<Array<User_Select_Column>>
    distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "user" */
export type User_Aggregate_Order_By = {
    count?: Maybe<Order_By>
    max?: Maybe<User_Max_Order_By>
    min?: Maybe<User_Min_Order_By>
}

/** input type for inserting array relation for remote table "user" */
export type User_Arr_Rel_Insert_Input = {
    data: Array<User_Insert_Input>
    on_conflict?: Maybe<User_On_Conflict>
}

/** Boolean expression to filter rows from the table "user". All fields are combined with a logical 'AND'. */
export type User_Bool_Exp = {
    _and?: Maybe<Array<Maybe<User_Bool_Exp>>>
    _not?: Maybe<User_Bool_Exp>
    _or?: Maybe<Array<Maybe<User_Bool_Exp>>>
    avatar?: Maybe<String_Comparison_Exp>
    bot?: Maybe<Boolean_Comparison_Exp>
    created_at?: Maybe<Timestamptz_Comparison_Exp>
    id?: Maybe<String_Comparison_Exp>
    locale?: Maybe<String_Comparison_Exp>
    members?: Maybe<Member_Bool_Exp>
    messages?: Maybe<Message_Bool_Exp>
    tag?: Maybe<String_Comparison_Exp>
    updated_at?: Maybe<Timestamptz_Comparison_Exp>
    user_created_at?: Maybe<Timestamptz_Comparison_Exp>
    username?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "user" */
export enum User_Constraint {
    /** unique or primary key constraint */
    UserPkey = 'user_pkey',
}

/** input type for inserting data into table "user" */
export type User_Insert_Input = {
    avatar?: Maybe<Scalars['String']>
    bot?: Maybe<Scalars['Boolean']>
    created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    locale?: Maybe<Scalars['String']>
    members?: Maybe<Member_Arr_Rel_Insert_Input>
    messages?: Maybe<Message_Arr_Rel_Insert_Input>
    tag?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    user_created_at?: Maybe<Scalars['timestamptz']>
    username?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type User_Max_Fields = {
    __typename?: 'user_max_fields'
    avatar?: Maybe<Scalars['String']>
    created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    locale?: Maybe<Scalars['String']>
    tag?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    user_created_at?: Maybe<Scalars['timestamptz']>
    username?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "user" */
export type User_Max_Order_By = {
    avatar?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    locale?: Maybe<Order_By>
    tag?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
    user_created_at?: Maybe<Order_By>
    username?: Maybe<Order_By>
}

/** aggregate min on columns */
export type User_Min_Fields = {
    __typename?: 'user_min_fields'
    avatar?: Maybe<Scalars['String']>
    created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    locale?: Maybe<Scalars['String']>
    tag?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    user_created_at?: Maybe<Scalars['timestamptz']>
    username?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "user" */
export type User_Min_Order_By = {
    avatar?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    locale?: Maybe<Order_By>
    tag?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
    user_created_at?: Maybe<Order_By>
    username?: Maybe<Order_By>
}

/** response of any mutation on the table "user" */
export type User_Mutation_Response = {
    __typename?: 'user_mutation_response'
    /** number of affected rows by the mutation */
    affected_rows: Scalars['Int']
    /** data of the affected rows by the mutation */
    returning: Array<User>
}

/** input type for inserting object relation for remote table "user" */
export type User_Obj_Rel_Insert_Input = {
    data: User_Insert_Input
    on_conflict?: Maybe<User_On_Conflict>
}

/** on conflict condition type for table "user" */
export type User_On_Conflict = {
    constraint: User_Constraint
    update_columns: Array<User_Update_Column>
    where?: Maybe<User_Bool_Exp>
}

/** ordering options when selecting data from "user" */
export type User_Order_By = {
    avatar?: Maybe<Order_By>
    bot?: Maybe<Order_By>
    created_at?: Maybe<Order_By>
    id?: Maybe<Order_By>
    locale?: Maybe<Order_By>
    members_aggregate?: Maybe<Member_Aggregate_Order_By>
    messages_aggregate?: Maybe<Message_Aggregate_Order_By>
    tag?: Maybe<Order_By>
    updated_at?: Maybe<Order_By>
    user_created_at?: Maybe<Order_By>
    username?: Maybe<Order_By>
}

/** primary key columns input for table: "user" */
export type User_Pk_Columns_Input = {
    id: Scalars['String']
}

/** select columns of table "user" */
export enum User_Select_Column {
    /** column name */
    Avatar = 'avatar',
    /** column name */
    Bot = 'bot',
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    Id = 'id',
    /** column name */
    Locale = 'locale',
    /** column name */
    Tag = 'tag',
    /** column name */
    UpdatedAt = 'updated_at',
    /** column name */
    UserCreatedAt = 'user_created_at',
    /** column name */
    Username = 'username',
}

/** input type for updating data in table "user" */
export type User_Set_Input = {
    avatar?: Maybe<Scalars['String']>
    bot?: Maybe<Scalars['Boolean']>
    created_at?: Maybe<Scalars['timestamptz']>
    id?: Maybe<Scalars['String']>
    locale?: Maybe<Scalars['String']>
    tag?: Maybe<Scalars['String']>
    updated_at?: Maybe<Scalars['timestamptz']>
    user_created_at?: Maybe<Scalars['timestamptz']>
    username?: Maybe<Scalars['String']>
}

/** update columns of table "user" */
export enum User_Update_Column {
    /** column name */
    Avatar = 'avatar',
    /** column name */
    Bot = 'bot',
    /** column name */
    CreatedAt = 'created_at',
    /** column name */
    Id = 'id',
    /** column name */
    Locale = 'locale',
    /** column name */
    Tag = 'tag',
    /** column name */
    UpdatedAt = 'updated_at',
    /** column name */
    UserCreatedAt = 'user_created_at',
    /** column name */
    Username = 'username',
}

export type QueryChannelByPkQueryVariables = {
    id: Scalars['String']
}

export type QueryChannelByPkQuery = { __typename?: 'query_root' } & {
    channel_by_pk?: Maybe<{ __typename?: 'channel' } & Pick<Channel, 'id'>>
}

export type InsertChannelMutationVariables = {
    channel_insert_input: Channel_Insert_Input
}

export type InsertChannelMutation = { __typename?: 'mutation_root' } & {
    insert_channel_one?: Maybe<{ __typename?: 'channel' } & Pick<Channel, 'id'>>
}

export type IsChannelHistoryCompleteQueryVariables = {
    id: Scalars['String']
}

export type IsChannelHistoryCompleteQuery = { __typename?: 'query_root' } & {
    channel_by_pk?: Maybe<
        { __typename?: 'channel' } & Pick<Channel, 'id' | 'history_complete'>
    >
}

export type QueryGuildByPkQueryVariables = {
    id: Scalars['String']
}

export type QueryGuildByPkQuery = { __typename?: 'query_root' } & {
    guild_by_pk?: Maybe<{ __typename?: 'guild' } & Pick<Guild, 'id'>>
}

export type InsertGuildMutationVariables = {
    guild_insert_input: Guild_Insert_Input
}

export type InsertGuildMutation = { __typename?: 'mutation_root' } & {
    insert_guild_one?: Maybe<{ __typename?: 'guild' } & Pick<Guild, 'id'>>
}

export type GetChannelOldestMessageQueryVariables = {
    channel_id: Scalars['String']
}

export type GetChannelOldestMessageQuery = { __typename?: 'query_root' } & {
    message: Array<{ __typename?: 'message' } & Pick<Message, 'id'>>
}

export type InsertMessagesMutationVariables = {
    messages: Array<Message_Insert_Input>
    on_conflict?: Maybe<Message_On_Conflict>
}

export type InsertMessagesMutation = { __typename?: 'mutation_root' } & {
    insert_message?: Maybe<
        { __typename?: 'message_mutation_response' } & Pick<
            Message_Mutation_Response,
            'affected_rows'
        >
    >
}

export type SetChannelHistoryCompleteMutationVariables = {
    channel_id: Scalars['String']
}

export type SetChannelHistoryCompleteMutation = {
    __typename?: 'mutation_root'
} & {
    update_channel_by_pk?: Maybe<
        { __typename?: 'channel' } & Pick<Channel, 'id'>
    >
}

export type QueryUserByPkQueryVariables = {
    id: Scalars['String']
}

export type QueryUserByPkQuery = { __typename?: 'query_root' } & {
    user_by_pk?: Maybe<{ __typename?: 'user' } & Pick<User, 'id'>>
}

export type InsertUserMutationVariables = {
    user_insert_input: User_Insert_Input
}

export type InsertUserMutation = { __typename?: 'mutation_root' } & {
    insert_user_one?: Maybe<{ __typename?: 'user' } & Pick<User, 'id'>>
}

export const QueryChannelByPkDocument = gql`
    query QueryChannelByPk($id: String!) {
        channel_by_pk(id: $id) {
            id
        }
    }
`
export type QueryChannelByPkQueryResult = ApolloReactCommon.QueryResult<
    QueryChannelByPkQuery,
    QueryChannelByPkQueryVariables
>
export const InsertChannelDocument = gql`
    mutation InsertChannel($channel_insert_input: channel_insert_input!) {
        insert_channel_one(object: $channel_insert_input) {
            id
        }
    }
`
export type InsertChannelMutationFn = ApolloReactCommon.MutationFunction<
    InsertChannelMutation,
    InsertChannelMutationVariables
>
export type InsertChannelMutationResult = ApolloReactCommon.MutationResult<
    InsertChannelMutation
>
export type InsertChannelMutationOptions = ApolloReactCommon.BaseMutationOptions<
    InsertChannelMutation,
    InsertChannelMutationVariables
>
export const IsChannelHistoryCompleteDocument = gql`
    query IsChannelHistoryComplete($id: String!) {
        channel_by_pk(id: $id) {
            id
            history_complete
        }
    }
`
export type IsChannelHistoryCompleteQueryResult = ApolloReactCommon.QueryResult<
    IsChannelHistoryCompleteQuery,
    IsChannelHistoryCompleteQueryVariables
>
export const QueryGuildByPkDocument = gql`
    query QueryGuildByPk($id: String!) {
        guild_by_pk(id: $id) {
            id
        }
    }
`
export type QueryGuildByPkQueryResult = ApolloReactCommon.QueryResult<
    QueryGuildByPkQuery,
    QueryGuildByPkQueryVariables
>
export const InsertGuildDocument = gql`
    mutation InsertGuild($guild_insert_input: guild_insert_input!) {
        insert_guild_one(object: $guild_insert_input) {
            id
        }
    }
`
export type InsertGuildMutationFn = ApolloReactCommon.MutationFunction<
    InsertGuildMutation,
    InsertGuildMutationVariables
>
export type InsertGuildMutationResult = ApolloReactCommon.MutationResult<
    InsertGuildMutation
>
export type InsertGuildMutationOptions = ApolloReactCommon.BaseMutationOptions<
    InsertGuildMutation,
    InsertGuildMutationVariables
>
export const GetChannelOldestMessageDocument = gql`
    query getChannelOldestMessage($channel_id: String!) {
        message(
            limit: 1
            where: { channel_id: { _eq: $channel_id } }
            order_by: { message_created_at: asc }
        ) {
            id
        }
    }
`
export type GetChannelOldestMessageQueryResult = ApolloReactCommon.QueryResult<
    GetChannelOldestMessageQuery,
    GetChannelOldestMessageQueryVariables
>
export const InsertMessagesDocument = gql`
    mutation InsertMessages(
        $messages: [message_insert_input!]!
        $on_conflict: message_on_conflict
    ) {
        insert_message(objects: $messages, on_conflict: $on_conflict) {
            affected_rows
        }
    }
`
export type InsertMessagesMutationFn = ApolloReactCommon.MutationFunction<
    InsertMessagesMutation,
    InsertMessagesMutationVariables
>
export type InsertMessagesMutationResult = ApolloReactCommon.MutationResult<
    InsertMessagesMutation
>
export type InsertMessagesMutationOptions = ApolloReactCommon.BaseMutationOptions<
    InsertMessagesMutation,
    InsertMessagesMutationVariables
>
export const SetChannelHistoryCompleteDocument = gql`
    mutation SetChannelHistoryComplete($channel_id: String!) {
        update_channel_by_pk(
            pk_columns: { id: $channel_id }
            _set: { history_complete: true }
        ) {
            id
        }
    }
`
export type SetChannelHistoryCompleteMutationFn = ApolloReactCommon.MutationFunction<
    SetChannelHistoryCompleteMutation,
    SetChannelHistoryCompleteMutationVariables
>
export type SetChannelHistoryCompleteMutationResult = ApolloReactCommon.MutationResult<
    SetChannelHistoryCompleteMutation
>
export type SetChannelHistoryCompleteMutationOptions = ApolloReactCommon.BaseMutationOptions<
    SetChannelHistoryCompleteMutation,
    SetChannelHistoryCompleteMutationVariables
>
export const QueryUserByPkDocument = gql`
    query QueryUserByPk($id: String!) {
        user_by_pk(id: $id) {
            id
        }
    }
`
export type QueryUserByPkQueryResult = ApolloReactCommon.QueryResult<
    QueryUserByPkQuery,
    QueryUserByPkQueryVariables
>
export const InsertUserDocument = gql`
    mutation InsertUser($user_insert_input: user_insert_input!) {
        insert_user_one(object: $user_insert_input) {
            id
        }
    }
`
export type InsertUserMutationFn = ApolloReactCommon.MutationFunction<
    InsertUserMutation,
    InsertUserMutationVariables
>
export type InsertUserMutationResult = ApolloReactCommon.MutationResult<
    InsertUserMutation
>
export type InsertUserMutationOptions = ApolloReactCommon.BaseMutationOptions<
    InsertUserMutation,
    InsertUserMutationVariables
>
