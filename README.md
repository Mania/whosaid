# Who said ?

## Requirements

-   git
-   docker and docker-compose
-   hasura cli

## Run

```shell script
git clone https://gitlab.com/Mania/whosaid.git
cd whosaid

cp config/config-example.yml config/config.yml
nano config/config.yml

docker-compose up -d postgres graphql-engine

cd hasura
hasura migrate apply

cd ..
docker-compose up -d whosaid
```
